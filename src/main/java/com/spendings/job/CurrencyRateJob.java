package com.spendings.job;

import com.spendings.entity.Rate;
import com.spendings.service.CurrencyService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import yahoofinance.YahooFinance;
import yahoofinance.quotes.fx.FxQuote;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by agubachev on 20.04.2016.
 */
@Component
public class CurrencyRateJob extends QuartzJobBean {

    @Autowired
    private CurrencyService currencyService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        if (!CollectionUtils.isEmpty(currencyService.getRates(new Date()))) {
            return;
        }
        List<Rate> rates = new ArrayList<>();
        List<String> codes = new ArrayList<>();
        Map<String, Long> symbols = new HashMap<>();
        currencyService.getAvailableCurrencies().forEach(currency -> {
            codes.add(currency.getAcronim() + "=X");
            symbols.put(currency.getAcronim(), currency.getId());
        });
        Map<String, FxQuote> quotes = null;
        try {
            quotes = YahooFinance.getFx(codes.toArray(new String[codes.size()]));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!CollectionUtils.isEmpty(quotes)) {
            quotes.forEach((code, fxQuote) -> {
                Long currencyId = symbols.get(code.split("=")[0]);
                Rate rate = new Rate(new Date(), fxQuote.getPrice(), currencyId, UUID.randomUUID());
                rates.add(rate);
            });
        }
        currencyService.updateDailyCurrencyRates(rates);
    }
}
