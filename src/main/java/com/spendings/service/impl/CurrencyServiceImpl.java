package com.spendings.service.impl;

import com.spendings.dao.CurrencyDao;
import com.spendings.entity.Currency;
import com.spendings.entity.Rate;
import com.spendings.service.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by agubachev on 20.04.2016.
 */
@Service("currencyService")
@Transactional(value = "txManager")
public class CurrencyServiceImpl implements CurrencyService {

    @Autowired
    private CurrencyDao currencyDao;

    @Override
    public void createCurrencyRate(Rate rate) {
        rate.setId(UUID.randomUUID());
        rate.setDate(new Date());
        currencyDao.createCurrencyRateRecord(rate);
    }

    @Override
    public Rate getCurrencyRate(Currency currency) {
        return getCurrencyRate(currency, new Date());
    }

    @Override
    public Rate getCurrencyRate(Currency currency, Date date) {
        return currencyDao.getCurrencyRate(currency, date);
    }

    @Override
    public void updateCurrencyRate(Rate rate) {

    }

    @Override
    public List<Currency> getAvailableCurrencies() {
        return currencyDao.getAvailableCurrencies();
    }

    @Override
    public boolean updateDailyCurrencyRates(List<Rate> rates) {
        return currencyDao.updateDailyCurrencyRates(rates);
    }

    @Override
    public List<Rate> getRates(Date date) {
        return currencyDao.getRates(date);
    }
}
