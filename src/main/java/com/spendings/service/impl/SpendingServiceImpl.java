package com.spendings.service.impl;

import com.spendings.dao.SpendingDao;
import com.spendings.entity.SpendingRecord;
import com.spendings.service.SpendingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

/**
 * Created by Sashko on 2/27/16.
 */
@Service("spendingService")
@Transactional(value = "txManager")
public class SpendingServiceImpl implements SpendingService {

    @Autowired
    private SpendingDao spendingDao;

    @Override
    public void createRecord(SpendingRecord spendingRecord) {
        spendingDao.createSpendingRecord(spendingRecord);
    }

    @Override
    public void updateRecord(SpendingRecord spendingRecord) {
        spendingDao.updateRecord(spendingRecord);
    }

    @Override
    public SpendingRecord getSpendingRecord(UUID recordId) {
        return spendingDao.getSpendingRecord(recordId);
    }

    @Override
    public void deleteSpendingRecordById(UUID recordId) {
        spendingDao.deleteSpendingRecordById(recordId);
    }

}
