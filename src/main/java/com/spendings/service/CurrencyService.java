package com.spendings.service;

import com.spendings.entity.Currency;
import com.spendings.entity.Rate;

import java.util.Date;
import java.util.List;

/**
 * Created by agubachev on 20.04.2016.
 */
public interface CurrencyService {

    void createCurrencyRate(Rate rate);

    Rate getCurrencyRate(Currency currency);

    Rate getCurrencyRate(Currency currency, Date date);

    void updateCurrencyRate(Rate rate);

    List<Currency> getAvailableCurrencies();

    boolean updateDailyCurrencyRates(List<Rate> rates);

    List<Rate> getRates(Date date);
}
