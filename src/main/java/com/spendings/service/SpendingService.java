package com.spendings.service;

import com.spendings.entity.SpendingRecord;

import java.util.UUID;

/**
 * Created by Sashko on 2/27/16.
 */
public interface SpendingService {

    void createRecord(SpendingRecord spendingRecord);

    void updateRecord(SpendingRecord spendingRecord);

    SpendingRecord getSpendingRecord(UUID recordId);

    void deleteSpendingRecordById(UUID recordId);
}
