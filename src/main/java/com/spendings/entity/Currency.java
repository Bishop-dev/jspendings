package com.spendings.entity;

/**
 * Created by Sashko on 2/6/16.
 */
public class Currency {

    private long id;
    // Dollar, Hrivna
    private String name;
    // USD, UAH
    private String acronim;
    // USA, Ukraine
    private String country;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAcronim() {
        return acronim;
    }

    public void setAcronim(String acronim) {
        this.acronim = acronim;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
