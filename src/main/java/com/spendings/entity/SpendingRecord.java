package com.spendings.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

/**
 * Created by Sashko on 2/6/16.
 */
public class SpendingRecord {
    private UUID id;
    private BigDecimal price;
    private long currencyId;
    private String name;
    private Date date;
    private long categoryId;
    private long cacheTypeId;
    private long userId;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(long currencyId) {
        this.currencyId = currencyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public long getCacheTypeId() {
        return cacheTypeId;
    }

    public void setCacheTypeId(long cacheTypeId) {
        this.cacheTypeId = cacheTypeId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
