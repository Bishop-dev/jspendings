package com.spendings.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

/**
 * Created by Sashko on 2/27/16.
 */
public class Rate {
    private UUID id;
    private long currencyId;
    private BigDecimal rate;
    private Date date;

    public Rate() {
    }

    public Rate(Date date, BigDecimal rate, long currencyId, UUID id) {
        this.date = date;
        this.rate = rate;
        this.currencyId = currencyId;
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public long getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(long currencyId) {
        this.currencyId = currencyId;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
