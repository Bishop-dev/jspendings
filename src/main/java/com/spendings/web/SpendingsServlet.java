package com.spendings.web;

import com.spendings.entity.SpendingRecord;
import com.spendings.service.SpendingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.UUID;

/**
 * Created by sashko on 2/8/16.
 */

@RestController
public class SpendingsServlet {

    @Autowired
    private SpendingService spendingService;

    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public ModelAndView index() {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("index");
        return mav;
    }

    @RequestMapping(method = RequestMethod.POST, path = "/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String addRecord(@RequestBody SpendingRecord record) {
        spendingService.createRecord(record);
        return "ololo";
    }

    @RequestMapping(method = RequestMethod.PUT, path = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public String updateRecord(@RequestBody SpendingRecord record) {
        spendingService.updateRecord(record);
        return "updated";
    }

    @RequestMapping(method = RequestMethod.GET, path = "/get/{uuid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody SpendingRecord getSpendingRecordById(@PathVariable("uuid") UUID recordId) {
        return spendingService.getSpendingRecord(recordId);
    }

    @RequestMapping(method = RequestMethod.DELETE, path = "/delete/{uuid}")
    public String deleteSpendingRecord(@PathVariable("uuid") UUID recordId) {
        spendingService.deleteSpendingRecordById(recordId);
        return "ololo";
    }


}
