package com.spendings.web;

import com.spendings.entity.Currency;
import com.spendings.entity.Rate;
import com.spendings.service.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

/**
 * Created by agubachev on 20.04.2016.
 */
@RestController
public class CurrencyServlet {

    @Autowired
    private CurrencyService currencyService;

    @RequestMapping(value = "/rate/currency", method = RequestMethod.GET)
    public Rate getCurrencyRate(@RequestParam(name = "currency", required = false) String currencyName,
                                @RequestParam(name = "id", required = false) Long id,
                                @RequestParam(name = "date", required = false) Long date) {
        Currency currency = new Currency();
        if (!StringUtils.isEmpty(currencyName)) {
            currency.setName(currencyName);
        }
        if (id != null && !id.equals(0)) {
            currency.setId(id);
        }
        if (date != null) {
            return currencyService.getCurrencyRate(currency, new Date(date));
        }
        return currencyService.getCurrencyRate(currency);
    }

    @RequestMapping(value = "/currencies", method = RequestMethod.GET)
    public List<Currency> getAvailableCurrencies() {
        return currencyService.getAvailableCurrencies();
    }

    @RequestMapping(value = "/rates", method = RequestMethod.GET)
    public List<Rate> getRates(@RequestParam(name = "date", required = false) Long timestamp) {
        Date date = new Date();
        if (timestamp != null && !timestamp.equals(0)) {
            date = new Date(timestamp);
        }
        return currencyService.getRates(date);
    }

}
