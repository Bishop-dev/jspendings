package com.spendings.db.mapping;

import com.spendings.entity.Rate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * Created by agubachev on 20.04.2016.
 */
public class CurrencyRateRowMapper implements RowMapper<Rate> {

    @Override
    public Rate mapRow(ResultSet rs, int i) throws SQLException {
        Rate rate = new Rate();
        rate.setCurrencyId(rs.getLong("currency_id"));
        rate.setDate(rs.getDate("rate_date"));
        rate.setId(UUID.fromString(rs.getString("rate_record_id")));
        rate.setRate(rs.getBigDecimal("rate"));
        return rate;
    }

}
