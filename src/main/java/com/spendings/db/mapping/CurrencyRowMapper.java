package com.spendings.db.mapping;

import com.spendings.entity.Currency;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by agubachev on 20.04.2016.
 */
public class CurrencyRowMapper implements RowMapper<Currency> {

    @Override
    public Currency mapRow(ResultSet rs, int i) throws SQLException {
        Currency currency = new Currency();
        currency.setId(rs.getInt("currency_id"));
        currency.setName(rs.getString("name"));
        currency.setAcronim(rs.getString("acronim"));
        currency.setCountry(rs.getString("country"));
        return currency;
    }

}
