package com.spendings.db.mapping;

import com.spendings.entity.SpendingRecord;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

/**
 * Created by Sashko on 2/27/16.
 */
public class SpendingRecordRowMapper implements RowMapper<SpendingRecord> {

    @Override
    public SpendingRecord mapRow(ResultSet rs, int rowNum) throws SQLException {
        SpendingRecord record = new SpendingRecord();
        record.setId(UUID.fromString(rs.getString("record_id")));
        record.setCacheTypeId(rs.getLong("cash_type"));
        record.setCategoryId(rs.getLong("category_id"));
        record.setCurrencyId(rs.getLong("currency_id"));
        record.setDate(rs.getDate("record_date"));
        record.setName(rs.getString("record_name"));
        record.setPrice(rs.getBigDecimal("price"));
        return record;
    }

}
