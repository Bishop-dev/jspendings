package com.spendings.db.query;

/**
 * Created by Sashko on 2/27/16.
 */
public interface SpendingQuery {

    String INSERT_SPENDING_RECORD = "INSERT INTO public.spending_record (record_id, " +
            "price, currency_id, record_name, record_date, category_id, cash_type) VALUES (:record_id, :price," +
            ":currency_id, :record_name, :record_date, :category_id, :cash_type)";

    String SELECT_SPENDING_RECORD_BY_ID = "SELECT * from public.spending_record WHERE record_id = :record_id";

    String UPDATE_SPENDING_RECORD_BY_ID = "UPDATE public.spending_record SET currency_id = :currency_id, " +
            "record_name = :record_name, record_date = :record_date, category_id = :category_id, cash_type = :cash_type " +
            " WHERE record_id = :record_id";

    String DELETE_SPENDING_RECORD_BY_ID = "DELETE from public.spending_record WHERE record_id = :record_id";

}
