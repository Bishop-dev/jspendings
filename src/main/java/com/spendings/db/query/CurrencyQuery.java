package com.spendings.db.query;

/**
 * Created by agubachev on 20.04.2016.
 */
public interface CurrencyQuery {

    String INSERT_CURRENCY_RATE = "INSERT INTO rate (rate_record_id, currency_id, rate, rate_date) VALUES " +
            "(:rate_record_id, :currency_id, :rate, :rate_date)";

    String SELECT_CURRENCY_RATE_BY_CURRENCY_ID_AND_DATE = "SELECT * FROM rate WHERE currency_id = :currency_id AND " +
            " (rate_date BETWEEN :start AND :end)";

    String SELECT_CURRENCY_RATE_IN_DAY = "SELECT * FROM rate WHERE rate_date BETWEEN :start AND :end";

    String SELECT_AVAILABLE_CURRENCIES = "SELECT * FROM currency";

}
