package com.spendings.dao.impl;

import com.spendings.dao.CurrencyDao;
import com.spendings.db.mapping.CurrencyRateRowMapper;
import com.spendings.db.mapping.CurrencyRowMapper;
import com.spendings.db.query.CurrencyQuery;
import com.spendings.entity.Currency;
import com.spendings.entity.Rate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by agubachev on 20.04.2016.
 */
@Repository
public class CurrencyDaoImpl implements CurrencyDao {

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public Rate getCurrencyRate(Currency currency, Date date) {
        MapSqlParameterSource params = getCurrencyRateMapping(currency, date);
        List<Rate> rates = namedParameterJdbcTemplate.query(CurrencyQuery.SELECT_CURRENCY_RATE_BY_CURRENCY_ID_AND_DATE,
                params, new CurrencyRateRowMapper());
        return CollectionUtils.isEmpty(rates) ? null : rates.get(0);
    }

    @Override
    public void createCurrencyRateRecord(Rate rate) {
        MapSqlParameterSource params = getCurrencyRateMapping(rate);
        namedParameterJdbcTemplate.update(CurrencyQuery.INSERT_CURRENCY_RATE, params);
    }

    @Override
    public List<Currency> getAvailableCurrencies() {
        return namedParameterJdbcTemplate.query(CurrencyQuery.SELECT_AVAILABLE_CURRENCIES, new CurrencyRowMapper());
    }

    @Override
    public boolean updateDailyCurrencyRates(List<Rate> rates) {
        int[] results = namedParameterJdbcTemplate.batchUpdate(CurrencyQuery.INSERT_CURRENCY_RATE,
                createCurrencyRateSqlParameterSource(rates));
        return rates.size() == Arrays.stream(results).sum();
    }

    @Override
    public List<Rate> getRates(Date date) {
        return namedParameterJdbcTemplate.query(CurrencyQuery.SELECT_CURRENCY_RATE_IN_DAY, getIntervalMapping(date),
                new CurrencyRateRowMapper());
    }

    private SqlParameterSource[] createCurrencyRateSqlParameterSource(List<Rate> offers) {
        return offers.stream().map(this::getCurrencyRateMapping).toArray(SqlParameterSource[]::new);
    }

    private MapSqlParameterSource getIntervalMapping(Date date) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("start", getStartDate(date));
        params.addValue("end", getEndDate(date));
        return params;
    }

    private MapSqlParameterSource getCurrencyRateMapping(Rate rate) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("rate_record_id", rate.getId());
        params.addValue("currency_id", rate.getCurrencyId());
        params.addValue("rate", rate.getRate());
        params.addValue("rate_date", rate.getDate());
        return params;
    }

    private MapSqlParameterSource getCurrencyRateMapping(Currency currency, Date date) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        Date start = getStartDate(date);
        Date end = getEndDate(date);
        params.addValue("currency_id", currency.getId());
        params.addValue("start", start);
        params.addValue("end", end);
        return params;
    }

    private Date getStartDate(Date date) {
        LocalDateTime ldt = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        return Date.from(ldt.truncatedTo(ChronoUnit.DAYS).atZone(ZoneId.systemDefault()).toInstant());
    }

    private Date getEndDate(Date date) {
        LocalDateTime ldt = LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        return Date.from(ldt.truncatedTo(ChronoUnit.DAYS).plusDays(1).atZone(ZoneId.systemDefault()).toInstant());
    }

}
