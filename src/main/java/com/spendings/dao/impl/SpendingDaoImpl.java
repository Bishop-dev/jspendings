package com.spendings.dao.impl;

import com.spendings.dao.SpendingDao;
import com.spendings.db.query.SpendingQuery;
import com.spendings.db.mapping.SpendingRecordRowMapper;
import com.spendings.entity.SpendingRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Sashko on 2/27/16.
 */
@Repository
public class SpendingDaoImpl implements SpendingDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public void createSpendingRecord(SpendingRecord record) {
        MapSqlParameterSource params = getFullSpendingRecordMapping(record);
        namedParameterJdbcTemplate.update(SpendingQuery.INSERT_SPENDING_RECORD, params);
    }

    @Override
    public void updateRecord(SpendingRecord spendingRecord) {
        MapSqlParameterSource params = getFullSpendingRecordMapping(spendingRecord);
        namedParameterJdbcTemplate.update(SpendingQuery.UPDATE_SPENDING_RECORD_BY_ID, params);
    }

    @Override
    public SpendingRecord getSpendingRecord(UUID recordId) {
        MapSqlParameterSource params = new MapSqlParameterSource("record_id", recordId);
        List<SpendingRecord> recordList = namedParameterJdbcTemplate.query(SpendingQuery.SELECT_SPENDING_RECORD_BY_ID, params,
                new SpendingRecordRowMapper());
        return CollectionUtils.isEmpty(recordList) ? null : recordList.get(0);
    }

    @Override
    public void deleteSpendingRecordById(UUID recordId) {
        MapSqlParameterSource params = new MapSqlParameterSource("record_id", recordId);
        namedParameterJdbcTemplate.update(SpendingQuery.DELETE_SPENDING_RECORD_BY_ID, params);
    }

    private MapSqlParameterSource getFullSpendingRecordMapping(SpendingRecord record) {
        MapSqlParameterSource params = new MapSqlParameterSource("price", record.getPrice());
        params.addValue("record_id", record.getId() == null ? UUID.randomUUID() : record.getId())
                .addValue("currency_id", record.getCurrencyId()).addValue("record_name", record.getName())
                .addValue("record_date", record.getDate() == null ? new Date() : record.getDate())
                .addValue("category_id", record.getCategoryId())
                .addValue("cash_type", record.getCacheTypeId());
        return params;
    }

}
