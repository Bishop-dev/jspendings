package com.spendings.dao;

import com.spendings.entity.SpendingRecord;

import java.util.UUID;

/**
 * Created by Sashko on 2/27/16.
 */
public interface SpendingDao {

    void createSpendingRecord(SpendingRecord spendingRecord);

    void updateRecord(SpendingRecord spendingRecord);

    SpendingRecord getSpendingRecord(UUID recordId);

    void deleteSpendingRecordById(UUID recordId);
}
