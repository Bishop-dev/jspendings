package com.spendings.dao;

import com.spendings.entity.Currency;
import com.spendings.entity.Rate;

import java.util.Date;
import java.util.List;

/**
 * Created by agubachev on 20.04.2016.
 */
public interface CurrencyDao {

    Rate getCurrencyRate(Currency currency, Date date);

    void createCurrencyRateRecord(Rate rate);

    List<Currency> getAvailableCurrencies();

    boolean updateDailyCurrencyRates(List<Rate> rates);

    List<Rate> getRates(Date date);
}
