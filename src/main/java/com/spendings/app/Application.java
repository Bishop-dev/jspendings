package com.spendings.app;

import com.spendings.web.SpendingsServlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by sashko on 2/8/16.
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        Object[] classes = {Application.class, SpendingsServlet.class, AppConfig.class, JdbcConfig.class};
        SpringApplication.run(classes, args);
    }
}
