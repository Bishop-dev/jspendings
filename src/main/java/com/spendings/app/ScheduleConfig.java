package com.spendings.app;

import com.spendings.job.CurrencyRateJob;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.spi.JobFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.scheduling.quartz.SimpleTriggerFactoryBean;

import java.util.Properties;

/**
 * Created by agubachev on 20.04.2016.
 */
@Configuration
@PropertySource(value = "classpath:application.properties")
public class ScheduleConfig {

    @Autowired
    private ApplicationContext applicationContext;

    @Value("${scheduler.poolSize}")
    private String schedulerPoolSize;

    @Value("#{new Integer('${currency.rates.fetch.interval}')}")
    private Integer fetchInterval;

    @Bean
    public JobFactory autowiringSpringBeanJobFactory() {
        AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
        jobFactory.setApplicationContext(applicationContext);
        return jobFactory;
    }

    @Bean(name = "applicationScheduler")
    public SchedulerFactoryBean schedulerFactory() {
        SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
        schedulerFactoryBean.setSchedulerName("applicationScheduler");
        schedulerFactoryBean.setApplicationContextSchedulerContextKey("applicationContext");
        schedulerFactoryBean.setJobFactory(autowiringSpringBeanJobFactory());
        schedulerFactoryBean.setAutoStartup(true);
        schedulerFactoryBean.setStartupDelay(0);
        schedulerFactoryBean.setTriggers(getTriggers());
        Properties quartzProps = new Properties();
        quartzProps.setProperty(SchedulerFactoryBean.PROP_THREAD_COUNT, schedulerPoolSize);
        schedulerFactoryBean.setQuartzProperties(quartzProps);
        return schedulerFactoryBean;
    }

    @Bean
    public SimpleTriggerFactoryBean currencyRatesRefreshTrigger() {
        SimpleTriggerFactoryBean currencyRatesRefreshTrigger = new SimpleTriggerFactoryBean();
        currencyRatesRefreshTrigger.setJobDetail(currencyRatesRefreshJobDetail().getObject());
        currencyRatesRefreshTrigger.setName("RefreshCurrencyRates_Trigger");
        currencyRatesRefreshTrigger.setGroup("REFRESH_CURRENCY_RATES_TRIGGER");
        currencyRatesRefreshTrigger.setRepeatInterval(fetchInterval);
        currencyRatesRefreshTrigger
                .setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_RESCHEDULE_NEXT_WITH_REMAINING_COUNT);
        currencyRatesRefreshTrigger.setStartDelay(0);
        return currencyRatesRefreshTrigger;
    }

    @Bean
    public JobDetailFactoryBean currencyRatesRefreshJobDetail() {
        JobDetailFactoryBean currencyRatesRefreshJobDetail = new JobDetailFactoryBean();
        currencyRatesRefreshJobDetail.setName("RefreshCurrencyRates_Job");
        currencyRatesRefreshJobDetail.setGroup("REFRESH_CURRENCY_RATES_JOB");
        currencyRatesRefreshJobDetail.setJobClass(CurrencyRateJob.class);
        currencyRatesRefreshJobDetail.setDurability(true);
        return currencyRatesRefreshJobDetail;
    }

    protected Trigger[] getTriggers() {
        return new Trigger[] { currencyRatesRefreshTrigger().getObject() };
    }

}
