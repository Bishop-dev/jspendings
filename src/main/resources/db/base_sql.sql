CREATE TABLE spending_record (
  record_id UUID PRIMARY KEY,
  price NUMERIC NOT NULL,
  currency_id INTEGER NOT NULL,
  record_name VARCHAR(64) NOT NULL,
  record_date TIMESTAMP NOT NULL,
  category_id INTEGER NOT NULL,
  cash_type INTEGER NOT NULL
);

CREATE TABLE currency (
  currency_id INTEGER PRIMARY KEY,
  name VARCHAR(16) NOT NULL,
  acronim VARCHAR(3) NOT NULL,
  country VARCHAR(32) NOT NULL
);

CREATE TABLE rate (
  rate_record_id UUID PRIMARY KEY,
  currency_id INTEGER NOT NULL,
  rate NUMERIC NOT NULL,
  rate_date TIMESTAMP NOT NULL
);

CREATE TABLE category (
  category_id INTEGER PRIMARY KEY,
  category_name VARCHAR(32)
);

CREATE TABLE cash_type (
  cash_type_id INTEGER PRIMARY KEY,
  cash_type_name VARCHAR(32) NOT NULL
);

insert into currency (currency_id, name, acronim, country) values (1, 'US Dollar', 'USD', 'USA');
insert into currency (currency_id, name, acronim, country) values (2, 'Ukrainian Hryvna', 'UAH', 'Ukraine');

insert into rate (rate_record_id, currency_id, rate, rate_date) values ('3fc32839-a853-4233-9fcd-35634c59a816', 2, 27.5, now());

insert into category (category_id, category_name) values (1, 'Commissions');
insert into category (category_id, category_name) values (2, 'Accommodation');